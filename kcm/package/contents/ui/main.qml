// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2018 Alexis Lopes Zubeta <contact@azubieta.net>
// SPDX-FileCopyrightText: 2020 Tomaz Canabrava <tcanabrava@kde.org>


import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2

import org.kde.kcm 1.2 as KCM

import org.kde.kirigami 2.14 as Kirigami
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

KCM.SimpleKCM {
    id: root

    implicitHeight: Kirigami.Units.gridUnit * 25
    implicitWidth: Kirigami.Units.gridUnit * 44

    ColumnLayout {
        anchors.fill: parent

        MobileForm.FormCard {
            Layout.fillWidth: true
            contentItem: ColumnLayout {
                spacing: 0

                MobileForm.FormCardHeader {
                    title: i18nd("kcm_prime", "Active Profile")
                }

                MobileForm.FormTextDelegate {
                    text: i18nd("kcm_prime", "Select the PRIME profile to use.")
                }

                MobileForm.FormCheckDelegate {
                    text: i18nd("kcm_prime", "Discrete GPU")
                    description: i18nd("kcm_prime", "Use the discrete GPU for all applications.")
                }

                MobileForm.FormCheckDelegate {
                    text: i18nd("kcm_prime", "Integrated GPU")
                    description: i18nd("kcm_prime", "Use the integrated GPU for all applications.")
                }

                MobileForm.FormCheckDelegate {
                    text: i18nd("kcm_prime", "On-demand")
                    description: i18nd("kcm_prime", "Use the integrated GPU for all applications, and the discrete GPU on demand.")
                }
            }
        }
    }
}

