// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2011 Craig Drummond <craig.p.drummond@gmail.com>
// SPDX-FileCopyrightText: 2018 Alexis Lopes Zubeta <contact@azubieta.net>
// SPDX-FileCopyrightText: 2020 Tomaz Canabrava <tcanabrava@kde.org>
/*
 * UFW KControl Module
 */

#ifndef KCM_H
#define KCM_H

#include "prime.h"
#include <KQuickAddons/ConfigModule>

class KCMPrime : public KQuickAddons::ConfigModule
{
    Q_OBJECT
    Q_PROPERTY(Prime *prime READ prime CONSTANT)

public:
    explicit KCMPrime(QObject *parent, const KPluginMetaData &metaData, const QVariantList &args);

    ~KCMPrime();
    void save() override;
    Prime *prime() const;

private:
    Prime *const m_prime;
};

#endif
