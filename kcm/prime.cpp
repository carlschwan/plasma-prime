// SPDX-FileCopyrigthText: 2023 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "prime.h"
#include <QFile>
#include <QSet>
#include <QHash>
#include <QProcess>

Prime::Prime(QObject *parent)
    : QObject(parent)
    , m_profile(fetchCurrentProfile())
{
}

Prime::Profile Prime::fetchCurrentProfile() const
{
    QProcess process;
    process.start(QStringLiteral("prime-select"), {QStringLiteral("query")});

    if (!process.waitForStarted()) {
        return None;
    }

    if (!process.waitForFinished()) {
        return None;
    }

    const auto line = QString::fromUtf8(process.readLine()).trimmed();
    QHash<QString, Profile> stringToProfile{
        { QStringLiteral("on-demand"), OnDemand },
        { QStringLiteral("nvidia"), Nvidia },
        { QStringLiteral("intel"), Intel },
    };

    if (stringToProfile.contains(line)) {
        return stringToProfile[line];
    }
    return None;
}

Prime::Profile Prime::profile() const
{
    return m_profile;
}

void Prime::setProfile(const Profile profile)
{
    if (m_profile == profile) {
        return;
    }

    if (!canTransact()) {
        return;
    }

    if (profile == None) {
        m_profile = profile;
        Q_EMIT profileChanged();
        return;
    }

    static QHash<Profile, QString> profileToString{
        { OnDemand, QStringLiteral("on-demand") },
        { Nvidia, QStringLiteral("nvidia") },
        { Intel, QStringLiteral("intel") },
    };

    QProcess process;

#ifdef BUILD_VANILLAOS
    process.start(QStringLiteral("pkexec"), {QStringLiteral("abroot"), QStringLiteral("exec"), QStringLiteral("-f"), QStringLiteral("prime-select"), profileToString[profile]});
#else
    process.start(QStringLiteral("prime-select"), {profileToString[profile]});
#endif

    if (!process.waitForStarted()) {
        return;
    }

    if (!process.waitForFinished()) {
        return;
    }

    m_profile = fetchCurrentProfile();

    Q_EMIT profileChanged();
}

bool Prime::canTransact() const
{
    return QFile::exists(QStringLiteral("/tmp/abroot-transactions.lock"));
}

bool Prime::isSupported() const
{
    if (m_profile == None) {
        return false;
    }

    if (!isLaptop()) {
        return false;
    }

    if (gpus().size() == 0) {
        return false;
    }

    return true;
}

bool Prime::isLaptop() const
{
    const auto path = QStringLiteral("/sys/devices/virtual/dmi/id/chassis_type");
    QFile file(path);
    if (!file.exists()) {
        return false;
    }

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return false;
    }

    auto ok = false;
    const auto chassisType = file.readLine().trimmed().toInt(&ok);
    if (!ok) {
        return false;
    }
    QSet<int> laptopChassis{8, 9, 10, 31};
    if (laptopChassis.contains(chassisType)) {
        return true;
    }
    return false;
}


QHash<Prime::GPUType, QString> Prime::gpus() const
{
    QProcess process;
    process.start(QStringLiteral("lspci"), {QStringLiteral("-k")});

    if (!process.waitForStarted()) {
        return {};
    }

    if (!process.waitForFinished()) {
        return {};
    }

    QHash<GPUBrand, QString> found;

    while (!process.atEnd()) {
        const auto line = QString::fromUtf8(process.readLine());


        if ((line.contains(QStringLiteral("VGA")) || line.contains(QStringLiteral("3D"))) && !line.contains(QStringLiteral("Non-VGA"))) {
            const QString gpu = line.split(QStringLiteral("controller:"))[1].trimmed();
            const auto lower = gpu.toLower();
            if (lower.contains(QStringLiteral("intel"))) {
                found[IntelCard] = gpu;
            } else if (lower.contains(QStringLiteral("nvidia"))) {
                found[NVideaCard] = gpu;
            } else if (lower.contains(QStringLiteral("amd")) || lower.contains(QStringLiteral("ati"))) {
                found[AMDCard] = gpu;
            }
        }
    }

    if (found.contains(IntelCard) && found.contains(NVideaCard)) {
        return {
            { Integrated, found[IntelCard] },
            { Discrete, found[NVideaCard] },
        };
    }

    if (found.contains(IntelCard) && found.contains(AMDCard)) {
        return {
            { Integrated, found[IntelCard] },
            { Discrete, found[AMDCard] },
        };
    }

    if (found.contains(NVideaCard) && found.contains(AMDCard)) {
        return {
            { Integrated, found[AMDCard] },
            { Discrete, found[NVideaCard] },
        };
    }

    return {};
}
