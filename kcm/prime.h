// SPDX-FileCopyrigthText: 2023 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QObject>

class Prime : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Profile profile READ profile WRITE setProfile NOTIFY profileChanged)
    Q_PROPERTY(bool canTransact READ canTransact CONSTANT)
    Q_PROPERTY(bool isSupported READ isSupported CONSTANT)

public:
    explicit Prime(QObject *parent = nullptr);

    enum Profile {
        Nvidia,
        Intel,
        OnDemand,
        None,
    };
    Q_ENUM(Profile);

    enum GPUType {
        Integrated,
        Discrete
    };

    enum GPUBrand {
        AMDCard,
        NVideaCard,
        IntelCard,
    };

    Profile profile() const;
    void setProfile(const Profile profile);

    bool canTransact() const;
    bool isSupported() const;

Q_SIGNALS:
    void profileChanged();

private:
    bool isLaptop() const;
    Profile fetchCurrentProfile() const;
    QHash<GPUType, QString> gpus() const;

    Profile m_profile;
    QString m_binary;
};