// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2011 Craig Drummond <craig.p.drummond@gmail.com>
// SPDX-FileCopyrightText: 2018 Alexis Lopes Zubeta <contact@azubieta.net>
// SPDX-FileCopyrightText: 2020 Tomaz Canabrava <tcanabrava@kde.org>
/*
 * UFW KControl Module
 */

#include "kcm.h"

#include <KAboutData>
#include <KJob>
#include <KLocalizedString>
#include <KPluginFactory>

#include "version.h"

K_PLUGIN_FACTORY_WITH_JSON(KCMPrimeFactory, "kcm_prime.json", registerPlugin<KCMPrime>();)

KCMPrime::KCMPrime(QObject *parent, const KPluginMetaData &metaData, const QVariantList &args)
    : KQuickAddons::ConfigModule(parent, metaData, args)
    , m_prime(new Prime(this))
{
    qmlRegisterAnonymousType<KJob>("org.kcm.firewall", 1);

    setButtons(Help | Apply);
}

KCMPrime::~KCMPrime()
{
}

void KCMPrime::save()
{
    //m_client->save();
}

Prime *KCMPrime::prime() const
{
    return m_prime;
}

#include "kcm.moc"
